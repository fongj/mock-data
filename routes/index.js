'use strict';

module.exports.init = function(app, origin) {
    require('./ogre.route').init(app, origin);
};
